var React = require('react');

module.exports = React.createClass({
  getInitialState: function() {
    return {data: []};
  },
  componentDidMount: function() {
    this.readBlabsFromAPI();
  },
  readBlabsFromAPI: function() {
    this.props.readFromAPI(this.props.origin + '/blabs', function(blabs) {
      this.setState({data: blabs});
    }.bind(this));
  },
  render: function() {
    alert(this.state.data)
    return (
      <div className="blabs-view">
        <li className="blab">
          <span className="blab-text">{this.state.data}</span>
        </li>
      </div>
    );
  }
});
