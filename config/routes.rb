Rails.application.routes.draw do
  match '*all', to: 'application#preflight', via: [:options]

  get 'current_user', to: 'application#current_user'
  get 'request_token/:id', to: 'tokens#request_token'
  get 'access_token/:id', to: 'tokens#access_token'
  get '/detail', to: 'blabs#detail'

  resources :blabs, only: [:index, :detail]
end
